# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'jamespdev'

set :scm, :git
set :repo_url, 'https://japellerano@bitbucket.org/japellerano/capistrano-fun.git'
set :deploy_to, '/var/www/sinatra'

set :ssh_options, {
  forward_agent: true,
  keys: '~/Downloads/ubuntu-ec2.pem'
}

set :log_level, :debug

set :keep_releases, 5

namespace :deploy do

  desc "Restart Application"
  task :restart do 
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join("tmp/restart.txt")
    end
  end

  after :finishing, "deploy:cleanup"
end