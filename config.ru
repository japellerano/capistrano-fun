require 'rubygems'
require 'sinatra'

set :enviornment, ENV['RACK_ENV'].to_sym
disable :run, :reload

require './app.rb'

run Sinatra::Application